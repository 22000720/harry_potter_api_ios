//
//  ViewController.swift
//  harry_potter_api
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Kingfisher
import  Alamofire

struct Ator : Decodable {
    let name : String
    let actor : String
    let image : String // url
}

class ViewController: UIViewController, UITableViewDataSource {
    var array_atores : [Ator]?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array_atores?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AtorCelula", for: indexPath) as! AtorCelula
        let ator = array_atores![indexPath.row]
        cell.AtorOutlet.text = ator.name
        cell.PersonagemOutlet.text = ator.actor
        cell.ImagemOutlet.kf.setImage(with: URL(string: ator.image))
        return cell
    }
    
    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        requestData()
        TableView.dataSource = self
    }
    
    
    private func requestData(){
        AF.request("https://hp-api.onrender.com/api/characters").responseDecodable(of: [Ator].self){
            response in
            if let ator_array = response.value{
                self.array_atores = ator_array
            }
            self.TableView.reloadData()
        }
    }
    
    private func setData(){
        
    }
}

